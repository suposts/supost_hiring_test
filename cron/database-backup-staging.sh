#!/usr/bin/env bash

# NOTE:
# - awscli must be installed on server - (sudo) pip install awscli
# - must run `aws configure`

BUCKET="supost-backup/databases"

echo "================================="
echo "== BACKUP DATABASES $(/bin/date +%Y-%m-%d) =="
echo "================================="

echo "** Started at: $(/bin/date)"

YEAR=`/bin/date +%Y`
MONTH=`/bin/date +%m`
DATE=`/bin/date +%Y%m%d`
FILENAME=supost_staging-$DATE.sql.gz

mkdir -p ~/mysql
echo "-> cd ~/mysql"
cd ~/mysql

echo "-> /usr/bin/find ./*.sql.gz -mtime +0 -exec rm {} \\;"
/usr/bin/find ./*.sql.gz -mtime +0 -exec rm {} \;

echo "-> Dumping database to sql file"
/usr/bin/mysqldump --opt --user=deploy --password="SUd3p10y!" supost_staging | /bin/gzip > $FILENAME

echo "-> /usr/local/bin/aws s3 cp $FILENAME s3://$BUCKET/$YEAR/$MONTH/$FILENAME"
/usr/local/bin/aws s3 cp $FILENAME s3://$BUCKET/$YEAR/$MONTH/$FILENAME

echo "** Finished at: $(/bin/date)"
