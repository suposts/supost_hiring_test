class S3AssetsSyncer
  def copy
    aws = validate_aws_command!
    options = default_options.join(' ')
    cmd = [aws, 's3', 'cp', options, local_path, s3_path, '--recursive'].join(' ')
    puts "=> Running: #{cmd}"
    system(cmd)
  end

  def sync
    aws = validate_aws_command!
    options = default_options.tap { |ary| ary << '--delete' if ENV['CLEANUP'] }.join(' ')
    cmd = [aws, 's3', 'sync', options, local_path, s3_path].join(' ')
    puts "=> Running: #{cmd}"
    system(cmd)
  end

  protected

  def validate_aws_command!
    cmd = `which aws 2>/dev/null`.chomp

    if cmd.blank?
      puts "ERROR: aws does not exist"
      exit 1
    end

    cmd
  end

  def default_options
    [
      '--output json',
      '--region us-east-1',
      '--acl public-read',
      '--exclude "*.DS_Store"',
      '--exclude "*uploads*"',
      '--exclude "*system*"'
    ]
  end

  def local_path
    File.join(Rails.root, 'public', '')
  end

  def s3_path
    if Rails.env.production?
      's3://supost-prod-assets/'
    else
      's3://supost-staging-assets/'
    end
  end

  public

  def self.copy
    new.copy
  end

  def self.sync
    new.sync
  end
end
