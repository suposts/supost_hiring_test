module EmailValidationHelper
  module ClassMethods
    def validates_email
      options = {
        :presence => true,
        :format   => { :with => EMAIL_REGEX, :allow_blank => :blank, :message => "must be a valid email address" },
        :length   => { :maximum => 255, :allow_blank => true }
      }
      validates :email, options
    end

    def have_valid_email?(email)
      email.present? && EMAIL_REGEX === email
    end
  end

  EMAIL_REGEX = /^[A-Z0-9._%-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i

  def have_valid_email?
    self.class.have_valid_email?(email)
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
end
