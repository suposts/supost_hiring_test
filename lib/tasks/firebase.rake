namespace :firebase do
  desc 'Import all data to firebase'
  task :import_all => :environment do
    FirebaseImporter.import_all
  end

  desc 'Import data to firebase'
  task :import => :environment do
    FirebaseImporter.import
  end

  desc 'Update data to firebase'
  task :update => :environment do
    FirebaseImporter.update
  end

  desc 'Import categories and subcategories'
  task :import_categories_and_subcategories => :environment do
    FirebaseImporter.import_categories_and_subcategories
  end
end
