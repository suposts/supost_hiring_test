namespace :assets do
  desc "Copy assets to S3"
  task :copy => :environment do
    S3AssetsSyncer.copy
  end

  task :cp => :copy

  desc "Sync assets to S3"
  task :sync => :environment do
    S3AssetsSyncer.sync
  end
end
