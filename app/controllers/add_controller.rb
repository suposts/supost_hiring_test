class AddController < ApplicationController
  before_filter :find_post, :only => [:preview, :confirm]

  def index
    if request.post?
      save_post
    else
      new_post
    end
  end

  def preview
    @post.enable_purchases = true
    @category = @post.category
    @subcategory = @post.subcategory
  end

  def confirm
    @post.update_attributes(:enable_purchases => params[:post].try(:[], :enable_purchases))
    unless Spammer.spam_email?(@post.email)
      store_email_in_cookies(@post)
      @post.send_activation_mail!
    end
    @post.mark_as_confirmed!
    redirect_to success_url
  end

  def success
  end

  private

  def find_post
    @post = Post.find(params[:id])
    redirect_to post_url(@post.id) and return if @post.ready?
  end

  def new_post
    if params[:cat].present?
      @category = Category.find_by_id(params[:cat])

      redirect_to add_post_url and return if @category.blank? || @category.inactive?
      redirect_to buy_job_post_link and return if @category.off_campus_jobs?

      if params[:sub].present?
        @subcategory = @category.subcategories.find_by_id(params[:sub])

        redirect_to add_post_url(@category.id) and return if @subcategory.blank?
      else
        @subcategories = @category.subcategories.all(:order => 'name')
      end

      @post = Post.new(
        :category    => @category,
        :subcategory => @subcategory,
        :email       => email_from_cookies
      )
    end
  end

  def save_post
    @category    = Category.find(params[:cat])
    @subcategory = Subcategory.find(params[:sub])

    @post = Post.new_with_default_values(post_params)
    @post.ip          = request_ip
    @post.category    = @category
    @post.subcategory = @subcategory

    if @post.save_with_photos(raw_photos)
      store_email_in_cookies(@post)
      redirect_to preview_post_url(@post.id) and return
    end
  end

  def post_params
    params[:post].slice(:name, :price, :body, :email)
  end

  def raw_photos
    params[:post].slice(:photo1, :photo2, :photo3, :photo4)
  end

  def buy_job_post_link
    "http://jobs.suposts.com"
  end

  def email_from_cookies
    cookies.signed[:_supost_poster]
  end

  def store_email_in_cookies(post)
    cookies.signed[:_supost_poster] = { :value => post.email, :expires => 3.months.from_now }
  end
end
