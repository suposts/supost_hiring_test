class Admin::EventsController < AdminController
  def index
    @events = Event.recent.page(params[:page]).per(20)
  end
end
