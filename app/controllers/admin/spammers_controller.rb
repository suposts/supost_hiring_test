class Admin::SpammersController < AdminController
  before_filter :find_spammer, :only => [:destroy]

  helper_method :can_update_post?

  def index
    @spammer = Spammer.new
    load_spammers
  end

  def create
    @spammer = Spammer.new spammer_params

    if @spammer.save
      redirect_to admin_spammers_url, :notice => "New spammer #{@spammer.email.inspect} was created successfully!"
    else
      load_spammers
      render :action => 'index'
    end
  end

  def destroy
    notice = "Spammer #{@spammer.email.inspect} was destroyed successfully!"
    @spammer.destroy
    redirect_to admin_spammers_url, :notice => notice
  end

  def posts
    @spammer = Spammer.find_by_id params[:id]
    redirect_to admin_spammers_url and return if @spammer.blank?
    @posts = load_posts(@spammer)
  end

  private

  def find_spammer
    @spammer = Spammer.find params[:id]
  end

  def load_spammers
    @spammers = Spammer.recent.page(params[:page]).per(25)
  end

  def spammer_params
    params[:spammer].slice(:email)
  end

  def load_posts(spammer)
    options = params.slice(:status).merge(:field => 'email', :value => spammer.email)
    Post.filter(options).includes(:category, :subcategory).page(params[:page]).per(20)
  end

  def can_update_post?(post)
    post.for_off_campus_jobs?
  end
end
