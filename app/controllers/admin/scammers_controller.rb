class Admin::ScammersController < AdminController
  before_filter :find_scammer, :only => [:destroy]

  def index
    @scammer = Scammer.new
    load_scammers
  end

  def create
    @scammer = Scammer.new scammer_params

    if @scammer.save
      redirect_to admin_scammers_url, :notice => "New scammer #{@scammer.email.inspect} was created successfully!"
    else
      load_scammers
      render :action => 'index'
    end
  end

  def destroy
    notice = "Scammer #{@scammer.email.inspect} was destroyed successfully!"
    @scammer.destroy
    redirect_to admin_scammers_url, :notice => notice
  end

  def messages
    @scammer = Scammer.find_by_id params[:id]
    redirect_to admin_scammers_url and return if @scammer.blank?
    @messages = Message.recent_messages(@scammer.email, params[:page])
  end

  def find
    set_default_finder_options
    @messages = ScammerFinder.new(finder_options).query
  end

  private

  def find_scammer
    @scammer = Scammer.find params[:id]
  end

  def load_scammers
    @scammers = Scammer.recent.page(params[:page]).per(25)
  end

  def scammer_params
    params[:scammer].slice(:email)
  end

  def set_default_finder_options
    params[:filter_by]    = 'month' if params[:filter_by].blank?
    params[:sort_by]      = 'recent_messages' if params[:sort_by].blank?
    params[:min_messages] = 10 if params[:min_messages].blank?
  end

  def finder_options
    params.slice(:filter_by, :sort_by, :min_messages)
  end
end
