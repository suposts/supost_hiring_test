class Admin::UsersController < AdminController
  before_filter :admin_required,    :except => [:profile, :password]
  before_filter :find_user,         :only   => [:show, :edit, :update, :destroy, :reset]
  before_filter :find_current_user, :only   => [:profile, :password]

  def index
    @users = User.page(params[:page]).per(10)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new user_params

    if @user.save
      redirect_to admin_users_url, :notice => 'New user was created successfully!'
    else
      render :action => 'new'
    end
  end

  def show
    render :action => 'edit'
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to admin_users_url, :notice => 'User was updated successfully!'
    else
      render :action => 'edit'
    end
  end

  def destroy
    notice = "User was #{@user.enabled? ? 'disabled' : 'enabled'} successfully!"
    @user.toggle!(:disabled)
    redirect_to admin_users_url, :notice => notice
  end

  def reset
    password = @user.reset_password!
    notice = "New password for <b>#{@user.email}</b> is <b>#{password}</b>.".html_safe
    redirect_to admin_users_url, :notice => notice
  end

  def profile
    if request.put? && @user.update_attributes(profile_params)
      redirect_to admin_profile_url, :notice => 'Your profile was updated successfully!'
    else
      render
    end
  end

  def password
    if request.put? && @user.change_password(params[:password], password_params)
      redirect_to admin_password_url, :notice => 'Your password was changed successfully!'
    else
      render
    end
  end

  private

  def admin_required
    redirect_to admin_root_url and return unless current_user && current_user.admin?
  end

  def find_user
    @user = User.find params[:id]
    redirect_to admin_users_url and return unless current_user.can_update?(@user)
  end

  def find_current_user
    @user = User.find current_user.id
  end

  def user_params
    params[:user].slice(:first_name, :last_name, :email, :password, :password_confirmation, :disabled)
  end

  def profile_params
    params[:user].slice(:first_name, :last_name, :email)
  end

  def password_params
    params[:user].slice(:password, :password_confirmation)
  end
end
