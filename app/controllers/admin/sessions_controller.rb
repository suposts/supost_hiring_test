class Admin::SessionsController < AdminController
  layout false

  before_filter :login_required, :only => :destroy

  def new
    session[:user_id] = nil
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      flash[:alert]     = nil
      redirect_to admin_posts_url, :notice => 'Logged in successfully!'
    else
      flash[:alert] = 'Email or password is incorrect'
      render :action => 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to :action => 'new'
  end
end
