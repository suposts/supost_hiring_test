class Admin::PaymentsController < AdminController
  def index
    @payments = Payment.recent.includes(:post).page(params[:page]).per(20)
  end
end
