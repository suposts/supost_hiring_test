class PostController < ApplicationController
  SWITCH_DATE = '2016-06-18'.to_date

  before_filter :find_post_by_access_token, :only => [:publish, :delete]
  before_filter :find_secured_post,         :only => [:legacy_publish, :legacy_delete]

  def index
    @post = Post.find params[:id]

    if @post.deleted?
      render_404
      return
    end

    @category = @post.category
    @subcategory = @post.subcategory

    @facebook = Rails.env.production?

    if request.post?
      if @post.reserved_or_purchased?
        redirect_to post_path(@post) and return
      end

      add_message
    else
      @payment = Payment.find_by_id(flash[:stripe_payment]) if flash[:stripe_payment].to_i > 0
      @message = Message.new :email => email_from_cookies
    end
  end

  def publish
    @post.mark_as_active!
    expire_home_page
  end

  def delete
    @post.mark_as_deleted!
    expire_home_page
  end

  # TODO: Remove this on 2016-10-01
  def legacy_publish
    publish
    render :action => 'publish'
  end

  # TODO: Remove this on 2016-10-01
  def legacy_delete
    delete
    render :action => 'delete'
  end

  private

  def find_post_by_access_token
    raise ActiveRecord::RecordNotFound.new if params[:access_token].blank?
    @post = Post.where(:access_token => params[:access_token]).first
    raise ActiveRecord::RecordNotFound.new unless @post
  end

  # TODO: Remove this on 2016-10-01
  def find_secured_post
    raise ActiveRecord::RecordNotFound.new if params[:id].blank? || params[:security_id].blank?
    options = [
      'id = ? AND security_id = ? AND created_at < ?',
      params[:id],
      params[:security_id],
      SWITCH_DATE
    ]
    @post = Post.where(options).first
    raise ActiveRecord::RecordNotFound.new unless @post
  end

  def add_message
    @message = @post.messages.new(message_params) do |m|
      m.ip = request_ip
    end

    if @message.save
      unless Scammer.scam_email?(@message.email)
        store_email_in_cookies(@message)
        @message.send_response_mail!
      end
      redirect_to post_url(@post.id), :notice => 'Message Sent!'
    end
  end

  def message_params
    params[:message].slice(:message, :email)
  end

  def email_from_cookies
    cookies.signed[:_supost_buyer] || cookies.signed[:_supost_poster]
  end

  def store_email_in_cookies(message)
    cookies.signed[:_supost_buyer] = { :value => message.email, :expires => 3.months.from_now }
  end
end
