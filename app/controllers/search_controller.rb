class SearchController < ApplicationController
  def index
    query          = params[:q].to_s.strip
    category_id    = params[:cat]
    subcategory_id = params[:sub]
    page           = params[:page].to_i > 0 ? params[:page].to_i : 1

    @posts = Post.search(query, category_id, subcategory_id, page)

    if query.blank? && subcategory_id
      @subcategory = Subcategory.find(subcategory_id)
      @category    = @subcategory.category
    elsif query.blank? && category_id
      @category = Category.find(category_id)
    end

    @page_title = page_title_for(@category, @subcategory, query)
    @category_code = @category.try(:code) || 'forsale'
  end

  private

  def page_title_for(category, subcategory, query)
    if query.blank?
      if subcategory
        subcategory.name
      elsif category
        category.short_name
      else
        'all posts'
      end
    elsif query.present?
      "Searched for '#{query}'"
    elsif subcategory
      subcategory.name
    elsif category
      category.short_name
    else
      ''
    end
  end
end
