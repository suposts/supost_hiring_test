class AdminController < ApplicationController
  layout 'admin'

  before_filter :login_required
  before_filter :require_http_authentication

  helper_method :current_user

  private

  def login_required
    redirect_to admin_login_url and return unless current_user
  end

  def require_http_authentication
    # login_procedure = lambda do |http_username, http_password|
    #   password = Digest::SHA1.hexdigest(http_password)
    #   http_username == 'supost' && password == '9951a8022c36fbbeaf05a5ae7e95f4dde58f18a0'
    # end

    # unless current_user && authenticate_or_request_with_http_basic("SUpost Admin", &login_procedure)
    #   return false
    # end

    true
  end

  def current_user
    @current_user ||= User.active.find_by_id(session[:user_id]) if session[:user_id]
  end
end
