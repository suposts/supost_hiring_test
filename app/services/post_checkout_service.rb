class PostCheckoutService
  attr_reader :post, :stripe_params, :payment, :charge

  def initialize(post, stripe_params)
    @post          = post
    @stripe_params = stripe_params
  end

  def checkout
    return false unless post.on_sale?

    @payment = nil

    post.transaction do
      @payment = Payment.create_payment!(post, stripe_params)

      if @payment.is_purchase?
        post.mark_as_purchased!
      else
        post.mark_as_reserved!
      end

      @charge = create_stripe_charge!(@payment)
    end

    @payment.try(:send_notifications!)

    true
  end

  private

  def create_stripe_charge!(supost_payment)
    metadata = post.stripe_metadata.merge(
      :payment_id => supost_payment.id,
      :buyer      => stripe_params[:stripeEmail],
      :time       => Time.now.to_s
    )

    charge = Stripe::Charge.create(
      :source      => stripe_params[:stripeToken],
      :description => post.stripe_description,
      :amount      => post.stripe_amount,
      :metadata    => metadata,
      :currency    => 'usd'
    )
  end
end
