module Admin::SpammersHelper
  def destroy_spammer_link(spammer)
    options = {
      :title   => 'Delete this spammer',
      :confirm => "Are you sure you want to delete #{spammer.email.inspect} from list of Spammers?",
      :method  => :delete
    }
    link_to image_tag('admin/delete.png'), admin_spammer_path(spammer), options
  end

  def posts_of_spammer_link(spammer, use_email = false)
    options = {
      :title  => 'View posts of this spammer',
      :target => '_blank',
      :class  => 'action'
    }

    text = if use_email
             spammer.email
           else
             image_tag('admin/info.png')
           end

    link_to text, posts_admin_spammer_path(spammer), options
  end
end
