module Admin::UsersHelper
  def edit_user_link(user)
    if current_user.can_update?(user)
      link_to user.name, edit_admin_user_path(user)
    else
      content_tag :span, user.name
    end
  end

  def reset_password_link(user)
    return unless current_user.can_update?(user)

    options = {
      :title   => 'Reset password for this user',
      :confirm => "Are you sure you want to reset password for #{user.email}?",
      :method  => :post
    }

    link_to image_tag('admin/reset.png'), reset_admin_user_path(user), options
  end

  def enable_user_link(user)
    return unless current_user.can_update?(user)

    action = user.enabled? ? 'Disable' : 'Enable'

    options = {
      :title   => "#{action} this user",
      :method  => :delete
    }

    path = if user.enabled?
             'admin/disable.png'
           else
             'admin/enable.png'
           end

    link_to image_tag(path), admin_user_path(user), options
  end
end
