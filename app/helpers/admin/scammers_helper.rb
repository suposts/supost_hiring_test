module Admin::ScammersHelper
  def destroy_scammer_link(scammer)
    options = {
      :title   => 'Delete this scammer',
      :confirm => "Are you sure you want to delete #{scammer.email.inspect} from list of Scammers?",
      :method  => :delete
    }
    link_to image_tag('admin/delete.png'), admin_scammer_path(scammer), options
  end

  def messages_of_scammer_link(scammer, use_email = false)
    options = {
      :title  => 'View messages of this scammer',
      :target => '_blank',
      :class  => 'action'
    }

    text = if use_email
             scammer.email
           else
             image_tag('admin/info.png')
           end

    link_to text, messages_admin_scammer_path(scammer), options
  end

  def similar_messages_link(message, use_email = false)
    options = {
      :title  => 'View similar messages of this email',
      :target => '_blank',
      :class  => 'action'
    }

    text = if use_email
             message.email
           else
             image_tag('admin/info.png')
           end

    link_to text, similar_admin_message_path(message), options
  end

  def report_scam_link(message)
    options = {
      :title   => 'Block this email',
      :confirm => "Are you sure you want to add #{message.email.inspect} to list of Scammers?",
      :method  => :post,
      :class   => 'action'
    }

    text = image_tag('admin/add.png')

    link_to text, report_admin_message_path(message), options
  end
end
