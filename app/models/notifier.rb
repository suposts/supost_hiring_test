class Notifier < ActionMailer::Base
  include ActionView::Helpers::NumberHelper

  NO_REPLY      = 'SUpost <response@mg.supost.com>'
  BCC_EMAIL     = 'aileen@suposts.com'
  BCC_PAY       = %w(bcc-pay@supost.com supost.emailbot@gmail.com)
  DEV_EMAIL     = 'nhphong1406@gmail.com'
  STAGING_EMAIL = 'gwientjes@gmail.com'

  helper_method :price_tag

  default :content_transfer_encoding => '7bit',
          :content_type              => 'text/plain',
          :charset                   => 'UTF-8',
          :from                      => NO_REPLY

  def complete_purchase_buyer(payment)
    find_payment_and_post(payment)

    options = {
      :to       => filter_email(@payment.buyer),
      :reply_to => @payment.seller,
      :subject  => "SUpost Purchase Success - Contact: #{@payment.seller}: #{@post.name}"
    }
    options.merge!(:bcc => BCC_PAY) if Rails.env.production?

    mail(options)
  end

  def complete_purchase_seller(payment)
    find_payment_and_post(payment)

    options = {
      :to       => filter_email(@payment.seller),
      :reply_to => @payment.buyer,
      :subject  => "SUpost Purchase Success - Contact: #{@payment.buyer}: #{@post.name}"
    }
    options.merge!(:bcc => BCC_PAY) if Rails.env.production?

    mail(options)
  end

  def complete_reservation_buyer(payment)
    find_payment_and_post(payment)

    options = {
      :to       => filter_email(@payment.buyer),
      :reply_to => @payment.seller,
      :subject  => "SUpost Reservation Success - Contact: #{@payment.seller}: #{@post.name}"
    }
    options.merge!(:bcc => BCC_PAY) if Rails.env.production?

    mail(options)
  end

  def complete_reservation_seller(payment)
    find_payment_and_post(payment)

    options = {
      :to       => filter_email(@payment.seller),
      :reply_to => @payment.buyer,
      :subject  => "SUpost Reservation Success - Contact: #{@payment.buyer}: #{@post.name}"
    }
    options.merge!(:bcc => BCC_PAY) if Rails.env.production?

    mail(options)
  end

  def activate(post)
    @post = post.is_a?(Post) ? post : Post.find(post)

    options = {
      :to      => filter_email(@post.email),
      :subject => "SUpost - Publish your post! #{@post.name}"
    }

    mail(options)
  end

  def response(message)
    @message = message.is_a?(Message) ? message : Message.find(message)
    @post    = @message.post

    options = {
      :from    => @message.email,
      :sender  => NO_REPLY,
      :to      => filter_email(@post.email),
      :subject => "SUpost - #{@message.email} response: #{@post.name}"
    }
    options.merge!(:bcc => BCC_EMAIL) if Rails.env.production?

    mail(options)
  end

  private

  def filter_email(email)
    if Rails.env.production?
      email
    elsif Rails.env.staging?
      [DEV_EMAIL, STAGING_EMAIL]
    else
      DEV_EMAIL
    end
  end

  def price_tag(price)
    number_to_currency(price, :precision => price.is_a?(Integer) ? 0 : 2)
  end

  def find_payment_and_post(payment)
    @payment = payment.is_a?(Payment) ? payment : Payment.find(payment)
    @post    = @payment.post
  end
end
