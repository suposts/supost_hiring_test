class Payment < ActiveRecord::Base
  PURCHASE    = 1
  RESERVATION = 2
  UNKNOWN     = 3

  CHECKOUT_TYPES = {
    PURCHASE    => 'Purchase',
    RESERVATION => 'Reservation',
    UNKNOWN     => 'Unknown'
  }

  belongs_to :post
  has_many   :sync_logs
  has_many   :events, :through => :sync_logs

  scope :recent, order('date DESC, created_at DESC')

  validates :post_id, :seller, :buyer, :checkout_type, :price, :amount, :rate, :date, :token,
            :presence => true

  delegate :name, :to => :post, :allow_nil => true, :prefix => true

  def stripe_synced?
    stripe_charge? && stripe_transaction?
  end
 
  def send_notifications!
    if Rails.env.development?
      send_notifications_now!
    else
      send_notifications_later!
    end
  end

  def send_notifications_now!
    mails =
      if is_purchase?
        [
          Notifier.complete_purchase_buyer(self),
          Notifier.complete_purchase_seller(self)
        ]
      else
        [
          Notifier.complete_reservation_buyer(self),
          Notifier.complete_reservation_seller(self)
        ]
      end
    mails.map(&:deliver)
  end

  def send_notifications_later!
    if is_purchase?
      [
        Notifier.delay.complete_purchase_buyer(self.id),
        Notifier.delay.complete_purchase_seller(self.id)
      ]
    else
      [
        Notifier.delay.complete_reservation_buyer(self.id),
        Notifier.delay.complete_reservation_seller(self.id)
      ]
    end
  end

  def rounded_price
    PriceHelper.round(price)
  end

  def rounded_amount
    PriceHelper.round(amount)
  end

  def rounded_fee
    fee = rounded_price * rate
    PriceHelper.round fee.round(2)
  end

  def display_date(fmt = '%a, %b %d, %Y')
    date.try(:strftime, fmt)
  end

  def display_checkout_type
    if is_purchase?
      'purchase'
    else
      'reservation'
    end
  end

  def human_checkout_type
    CHECKOUT_TYPES[checkout_type]
  end

  def is_purchase?
    checkout_type == PURCHASE
  end

  def is_reservation?
    checkout_type == RESERVATION
  end

  def self.parse_checkout_type(checkout_type)
    if checkout_type == :purchase
      PURCHASE
    elsif checkout_type == :reservation
      RESERVATION
    else
      UNKNOWN
    end
  end

  def self.create_payment!(post, stripe_params)
    payment_params = {
      :post_id       => post.id,
      :seller        => post.email,
      :buyer         => stripe_params[:stripeEmail],
      :price         => post.rounded_price,
      :amount        => PriceHelper.to_dollars(post.stripe_amount),
      :rate          => post.rate,
      :date          => Time.now.to_date,
      :token         => stripe_params[:stripeToken],
      :checkout_type => Payment.parse_checkout_type(post.stripe_checkout_type)
    }
    Payment.create!(payment_params)
  end

  def self.sync_data!(event)
    return false unless event.charge_succeeded?

    data_object = event.data_object
    return false if data_object.blank?

    metadata = data_object['metadata'] || {}
    return false if metadata.blank?

    post_id = metadata['post_id']
    return false if post_id.blank? || post_id.to_i <= 0

    post = Post.find_by_id(post_id)
    return false if post.nil?

    payment_id = metadata['payment_id']
    return false if payment_id.blank? || payment_id.to_i == 0

    payment = post.payments.find_by_id(payment_id.to_i)
    return false if payment.nil?

    SyncLog.create(:payment => payment, :event => event)

    return false if payment.stripe_synced?

    source = data_object['source'] || {}

    payment.stripe_charge      = data_object['id']
    payment.stripe_transaction = data_object['balance_transaction']
    payment.stripe_source      = source['id']
    payment.save!
  end
end
