class ScammerFinder
  FILTER_BY_OPTIONS = {
    'This Week'    => 'week',
    'This Month'   => 'month',
    'This Quarter' => 'quarter',
    'This Year'    => 'year'
  }

  SORT_BY_OPTIONS = {
    'Last Message Sent' => 'created_at',
    'Recent Messages'   => 'recent_messages',
    'Total Messages'    => 'total_messages'
  }

  MESSAGE_RANGE = 10..200

  attr_reader :options

  def initialize(options = {})
    @options = options.symbolize_keys
  end

  def query
    start_time   = parse_start_time(options[:filter_by])
    min_messages = parse_min_messages(options[:min_messages])
    order_by     = parse_sort_by(options[:sort_by])

    values = {
      :start_time   => start_time,
      :pattern      => '(stanford.edu)|(stanfordalumni.org)|(.edu)$',
      :min_messages => min_messages
    }

    sql = <<-SQL
      SELECT * FROM (
        SELECT email, MAX(id) AS id, COUNT(DISTINCT id) AS total_messages, SUM(created_at > :start_time) AS recent_messages, MAX(created_at) AS created_at
        FROM messages
        WHERE email NOT REGEXP :pattern
        GROUP BY email
        HAVING COUNT(DISTINCT id) > :min_messages
      ) x
      WHERE created_at > :start_time
        AND NOT EXISTS( SELECT id FROM scammers WHERE scammers.email = x.email )
      ORDER BY #{order_by}
    SQL
    Message.find_by_sql([sql, values])
  end

  protected

  def parse_start_time(filter = nil)
    today = Time.zone.now.to_date
    if filter == 'year'
      today.beginning_of_year
    elsif filter == 'quarter'
      today.beginning_of_quarter
    elsif filter == 'week'
      today.beginning_of_week
    else
      today.beginning_of_month
    end
  end

  def parse_sort_by(sort_by = nil)
    if sort_by == 'created_at'
      'created_at DESC'
    elsif sort_by == 'total_messages'
      'total_messages DESC, recent_messages DESC, created_at DESC'
    else
      'recent_messages DESC, created_at DESC'
    end
  end

  def parse_min_messages(min_messages = nil)
    min = min_messages.to_i

    if min < MESSAGE_RANGE.begin
      min = MESSAGE_RANGE.begin
    elsif min > MESSAGE_RANGE.end
      min = MESSAGE_RANGE.end
    end

    min
  end
end
