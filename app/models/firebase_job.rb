class FirebaseJob < Struct.new(:post_id)
  def perform
    posts = Post.where(:id => post_id).all
    FirebaseImporter.sync(posts)
  end
end
