class Message < ActiveRecord::Base
  EMAIL_REGEX = /^[A-Z0-9._%-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i

  belongs_to :post

  before_validation :remove_whitespaces!

  validates_presence_of :message
  validates_length_of   :message, :maximum => 32000, :allow_nil => nil, :allow_blank => true

  validates_presence_of :email
  validates_format_of   :email, :with => EMAIL_REGEX, :allow_nil => nil, :allow_blank => true
  validates_length_of   :email, :maximum => 255, :allow_nil => nil, :allow_blank => true

  scope :recent, order('messages.created_at DESC')

  def truncated_email(length = 20)
    if email.present?
      chars = email.mb_chars
      if chars.length <= length
        email
      else
        chars[0...length] + '...'
      end
    else
      ''
    end
  end

  def self.recent_messages(email, page)
    self.includes(:post)
      .where(:email => email)
      .recent
      .page(page)
      .per(20)
  end

  def display_message
    message.to_s.gsub(/[‘’]/, "'").gsub(/[“”]/, '"')
  end

  def send_response_mail!
    if Rails.env.development?
      send_response_mail_now!
    else
      send_response_mail_later!
    end
  end

  def send_response_mail_now!
    Notifier.response(self).deliver
  end

  def send_response_mail_later!
    Notifier.delay.response(self.id)
  end

  def display_created_at
    (created_at || updated_at || Time.now).strftime("%a, %b %d, %Y %I:%M %p")
  end

  protected

  def remove_whitespaces!
    self.email = self.email.strip if self.email.respond_to?(:strip)
  end
end
