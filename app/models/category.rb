class Category < ActiveRecord::Base
  # All catgories
  CAMPUS_JOBS      = 1
  OFF_CAMPUS_JOBS  = 2
  HOUSING_OFFERING = 3
  HOUSING_NEED     = 4
  FOR_SALE         = 5
  RESUMES          = 6
  SERVICES         = 7
  PERSONALS        = 8
  COMMUNITY        = 9
  EVENTS           = 10

  ICONS = {
    CAMPUS_JOBS      => 'campus-jobs-icon',
    OFF_CAMPUS_JOBS  => 'off-campus-jobs-icon',
    HOUSING_OFFERING => 'housing-icon',
    HOUSING_NEED     => 'housing-icon',
    FOR_SALE         => 'for-sale-icon',
    RESUMES          => 'resumes-icon',
    SERVICES         => 'services-icon',
    PERSONALS        => 'personals-icon',
    COMMUNITY        => 'community-icon',
    EVENTS           => 'events-icons'
  }

  CODES = {
    CAMPUS_JOBS      => 'campus_jobs',
    OFF_CAMPUS_JOBS  => 'off_campus_jobs',
    HOUSING_OFFERING => 'housing',
    HOUSING_NEED     => 'housing',
    FOR_SALE         => 'forsale',
    RESUMES          => 'resumes',
    SERVICES         => 'services',
    PERSONALS        => 'personals',
    COMMUNITY        => 'community',
    EVENTS           => 'events'
  }

  attr_accessor :columns

  has_many :subcategories

  def price_required?
    for_sale? || housing_offering?
  end

  def for_sale?
    id == FOR_SALE
  end

  def off_campus_jobs?
    id == OFF_CAMPUS_JOBS
  end

  def housing_offering?
    id == HOUSING_OFFERING
  end

  def inactive?
    id == RESUMES || id == EVENTS
  end

  def icon
    ICONS[id || FOR_SALE]
  end

  def code
    CODES[id || FOR_SALE]
  end

  def display_name
    short_name || name
  end

  def self.active_category_ids
    @active_category_ids ||= mappings.keys
  end

  def self.active_categories
    @active_categories ||= mappings.values
  end

  def self.first_active_categories
    @first_active_categories ||= [FOR_SALE, PERSONALS, HOUSING_OFFERING].map { |id| mappings[id] }
  end

  def self.second_active_categories
    @second_active_categories ||= [OFF_CAMPUS_JOBS, CAMPUS_JOBS, COMMUNITY, SERVICES].map { |id| mappings[id] }
  end

  def self.mappings
    @mappings ||=
      begin
        categories                   = ActiveSupport::OrderedHash.new
        categories[FOR_SALE]         = Category.fake(:id => FOR_SALE, :name => 'for sale', :columns => for_sale_subcategories)
        categories[PERSONALS]        = Category.fake(:id => PERSONALS, :name => 'personals', :columns => personals_subcategories)
        categories[HOUSING_OFFERING] = Category.fake(:id => HOUSING_OFFERING, :name => 'housing', :columns => housing_subcategories)
        categories[OFF_CAMPUS_JOBS]  = Category.fake(:id => OFF_CAMPUS_JOBS, :name => 'jobs off-campus', :short_name => 'jobs', :columns => [])
        categories[CAMPUS_JOBS]      = Category.fake(:id => CAMPUS_JOBS, :name => 'campus job', :columns => campus_jobs_subcategories)
        categories[COMMUNITY]        = Category.fake(:id => COMMUNITY, :name => 'community', :columns => community_subcategories)
        categories[SERVICES]         = Category.fake(:id => SERVICES, :name => 'services', :columns => services_subcategories)
        categories
      end
  end

  def self.fake(values = {})
    new(values.slice(:name, :short_name, :columns)) do |category|
      category.id = values[:id] if values[:id].present?
    end
  end

  def self.for_sale_subcategories
    [
      [
        [155, 'art+shop'],
        [3,   'barter'],
        [4,   'bikes'],
        [6,   'books'],
        [7,   'cars'],
        [8,   'cds/dvds'],
        [9,   'clothes+acc'],
        [11,  'computers']
      ],
      [
        [12,  'electronics'],
        [13,  'free'],
        [14,  'furniture'],
        [156, 'games'],
        [17,  'household'],
        [23,  'tickets'],
        [18,  'wanted'],
        [151, 'general'],
      ]
    ]
  end

  def self.personals_subcategories
    [
      [
        [130, 'friendship'],
        [131, 'girl wants girl'],
        [132, 'girl wants guy'],
        [133, 'guy wants girl'],
        [134, 'guy wants guy'],
        [135, 'general']
      ]
    ]
  end

  def self.housing_subcategories
    [
      [
        [60, 'apts/housing'],
        # [61, 'housing swap'],
        [4,  'housing wanted', true],
        [59, 'rooms/shared'],
        # [63, 'storage/parking'],
        [66, 'sublets/temporary']
      ]
    ]
  end

  def self.off_campus_jobs_subcategories
    [
      [
        [161, 'full-time'],
        [160, 'paid interns']
      ],
      [
        [159, 'part-time'],
        [157, 'unpaid'],
        [158, 'general']
      ]
    ]
  end

  def self.campus_jobs_subcategories
    [
      [
        [143, 'admin'],
        [144, 'research']
      ],
      [
        [142, 'teaching'],
        [141, 'tutoring'],
        [146, 'general']
      ]
    ]
  end

  def self.community_subcategories
    [
      [
        [90,  'activities'],
        # [150, 'advice'],
        [92,  'childcare'],
        [102, 'classes'],
        # [154, 'funny'], 
        # [94,  'groups'],
        [96,  'lost+found']
      ],
      [
        [95,  'news+views'],
        # [99,  'politics'],
        # [128, 'religion'],
        [149, 'rideshare'],
        [101, 'volunteers'],
        [93,  'general']
      ]
    ]
  end

  def self.services_subcategories
    [
      [
        [73, 'computer']
      ],
      [
        [80,  'tutoring'],
        [147, 'general']
      ]
    ]
  end
end
