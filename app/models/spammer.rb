class Spammer < ActiveRecord::Base
  include EmailValidationHelper

  before_validation :process_email

  validates_email
  validates_uniqueness_of :email,
                          :case_sensitive => false,
                          :allow_blank => true

  scope :recent, :order => 'created_at DESC'

  def display_created_at
    created_at.strftime("%a, %b %d, %Y %I:%M %p")
  end

  def self.spam_email?(email)
    email = process_email(email)

    if have_valid_email?(email)
      where('LOWER(email) = ?', email.downcase).exists?
    else
      true
    end
  end

  def self.process_email(email)
    email = email.strip if email.respond_to?(:strip)
    email
  end

  private

  def process_email
    self.email = self.class.process_email(self.email)
  end
end
