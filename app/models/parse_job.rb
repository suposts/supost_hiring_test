class ParseJob < Struct.new(:post_id)
  def perform
    posts = Post.where(:id => post_id).all
    ParseImporter.sync(posts)
  end
end
