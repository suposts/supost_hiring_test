# SUpost: Marketplace for Stanford Students

## Development

Please view `DEVELOPMENT.md` for more details.

## Setup new droplet

Please view `supost-ubuntu-14.04-setup.sh` for more details.

## Migrate SUpost to new droplet

Please view `DEPLOYMENT.md` for more details.
