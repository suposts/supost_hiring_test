# SUpost Development (macOS only)

## Install Homebrew

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## Install MySQL

    $ brew install mysql

To have launchd start mysql now and restart at login:
    
    $ brew services start mysql

Or, if you don't want/need a background service you can just run:
    
    $ mysql.server start

To connect run:

    $ mysql -uroot

## Install imagemagick

    $ brew install imagemagick

## Rbenv and its brothers

Install rbenv using git

    $ git clone https://github.com/sstephenson/rbenv.git       ~/.rbenv
    $ git clone https://github.com/sstephenson/ruby-build.git  ~/.rbenv/plugins/ruby-build
    $ git clone https://github.com/jf/rbenv-gemset.git         ~/.rbenv/plugins/rbenv-gemset
    $ git clone https://github.com/rkh/rbenv-update.git        ~/.rbenv/plugins/rbenv-update

Setup PATH variable, add two following lines to ~/.bashrc, then restart your terminal

    export PATH=$HOME/.rbenv/bin:$PATH
    eval "$(rbenv init -)"

Install Ruby 2.0.0-p648

    $ rbenv install 2.0.0-p648

## Project setup

Restart your terminal, then clone supost project

    $ git clone git@bitbucket.org:suposts/supost.git

Copy dev configurations

    $ cd supost
    $ cp config/application.yml.example config/application.yml

Install bundler v1.10.6, and project's dependencies

    $ cd supost
    $ rbenv local 2.0.0-p648
    $ ruby -v # ruby 2.0.0p648 (2015-12-16 revision 53162) [x86_64-darwin15.4.0]
    $ gem install bundler -v 1.10.6
    $ bundle install --path gems # install dependencies to gems folder

Setup development database

    $ cd supost
    $ bundle exec rake db:create db:migrate
    $ bundle exec rake db:seed
    $ bundle exec rake db:populate 

Start server

    $ script/puma

## Deployment

    $ cd supost
    $ bundle exec cap staging deploy # deploy new code to staging server
    $ bundle exec cap production deploy # deploy new code to production server

## Sites

[Staging](http://104.216.116.132:8080) | [Production](http://supost.com)
