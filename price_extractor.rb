class PriceExtractor
  def self.match_price(name)
    md   = nil
    md ||= name.match(/^\[(\d+[\,\.]\d+\$)\][\s\:\_]/)
    md ||= name.match(/^\[(\d+\$)\][\s\:\_]/)
    md ||= name.match(/^\((\d+[\,\.]\d+\$)\)[\s\:\_]/)
    md ||= name.match(/^\((\d+\$)\)[\s\:\_]/)
    md ||= name.match(/^(\d+[\,\.]\d+\$)[\s\:\_]/)
    md ||= name.match(/^(\d+\$)[\s\:\_]/)
    md ||= name.match(/^(\$\d+[\,\.]\d+)[\s\:\_]/)
    md ||= name.match(/^(\$\d+)[\s\:\_]/)
    md ||= name.match(/^(\$\d+)o\.?b\.?\o\.?[\s\-]/)
    md ||= name.match(/[\s\-\.]\((\$\d+[\,\.]\d+)\)\!*$/)
    md ||= name.match(/[\s\-\.]\((\$\d+)\)\!*$/)
    md ||= name.match(/[\s\-\.]\[(\$\d+[\,\.]\d+)\]\!*$/)
    md ||= name.match(/[\s\-\.]\[(\$\d+)\]\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+[\,\.]\d+)\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+)\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+[\,\.]\d+)\.?$/)
    md ||= name.match(/[\s\-\.](\$\d+)\.?$/)
    md ||= name.match(/[\s\-\.](\d+[\,\.]\d+\s\$)$/)
    md ||= name.match(/[\s\-\.](\d+\s\$)$/)
    md ||= name.match(/[\s\-\.]\(like\snew\s(\$\d+)\)$/)
    md ||= name.match(/[\s\-\.](\$\d+[\,\.]\d+)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+[\,\.]\d+)[\,\/]o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\$\d+)[\,\/]o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\$\s\d+[\,\.]\d+)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\$\s\d+)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\d+[\,\.]\d+\$)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\d+\$)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\d+[\,\.]\d+\s\$)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\d+\s\$)\s*o\.?b\.?o\.?\!*$/)
    md ||= name.match(/[\s\-\.](\d+\$)\sor\so\.?b\.?o\.?$/)
    md ||= name.match(/[\s\-\.]\((\$\d+[\,\.]\d+)\s*o\.?b\.?o\.?\)\!*$/)
    md ||= name.match(/[\s\-\.]\((\$\d+)\s*o\.?b\.?o\.?\)\!*$/)
    md ||= name.match(/[\s\-\.]\((\d+[\,\.]\d+\$)\s*o\.?b\.?o\.?\)\!*$/)
    md ||= name.match(/[\s\-\.]\((\d+\$)\s*o\.?b\.?o\.?\)\!*$/)
    md ||= name.match(/\s\((\$\d+)\)$/)
    md ||= name.match(/thanksgiving.+\s(\$\d+)\,\s*obo\)$/)
    md ||= name.match(/\s(\d+)\s*usd$/)
    md ||= name.match(/\s\((\d+)usd\)$/)
    md ||= name.match(/\s(\d+)\s*usd\sonly$/)
    md ||= name.match(/\s(\d+\$)\sreward\scard$/)
    md ||= name.match(/\s(\d+\$)\ssuper\scash$/)
    md ||= name.match(/\s\((\d+[\,\.]\d+\$)[\s\/]each\.?\)$/)
    md ||= name.match(/\s\((\d+\$)[\s\/]each\.?\)$/)
    md ||= name.match(/\s\((\$\d+[\,\.]\d+)[\s\/]each\.?\)$/)
    md ||= name.match(/\s\((\$\d+)[\s\/]each\.?\)$/)
    md ||= name.match(/\s(\d+[\,\.]\d+\$)[\s\/]each\.?$/)
    md ||= name.match(/\s(\d+\$)[\s\/]each\.?$/)
    md ||= name.match(/\s(\$\d+[\,\.]\d+)[\s\/]each\.?$/)
    md ||= name.match(/\s(\$\d+)[\s\/]each\.?$/)
    md ||= name.match(/\s(\d+[\,\.]\d+\$)[\s\/]ea$/)
    md ||= name.match(/\s(\d+\$)[\s\/]ea$/)
    md ||= name.match(/\s(\$\d+[\,\.]\d+)[\s\/]ea$/)
    md ||= name.match(/\s(\$\d+)[\s\/]ea$/)
    md ||= name.match(/\s\((\d+[\,\.]\d+\$)[\s\/]ea\)$/)
    md ||= name.match(/\s\((\d+\$)[\s\/]ea\)$/)
    md ||= name.match(/\s\((\$\d+[,\.]\d+)[\s\/]ea\)$/)
    md ||= name.match(/\s\((\$\d+)[\s\/]ea\)$/)
    md ||= name.match(/\s(\$\d+)\scash$/)
    md ||= name.match(/\s(\$\d+)\sfor\sall$/)
    md ||= name.match(/\s\(used\s(\d+\$)\)$/)
    md ||= name.match(/\s\(used\s(\$\d+)\)$/)
    md ||= name.match(/\s(\$\d+)\s\(.+\)$/)
    md ||= name.match(/\s(\d+\$)\sonly\!*$/)
    md ||= name.match(/\s(\$\d+)\sonly\!*$/)
    md ||= name.match(/\s(\$\d+)\soff\!+$/)
    md ||= name.match(/\s(\d+\$)\soff\!+$/)
    md ||= name.match(/\s(\d+\$)\s\!+$/)
    md ||= name.match(/\s(\$\d+)\s\!+$/)
    md ||= name.match(/\s(\$\d+)\s\*+$/)
    md ||= name.match(/\s(\d+\$)\s\*+$/)
    md ||= name.match(/[\s\-](\$\s\d+[\,\.]\d+)\!*$/)
    md ||= name.match(/[\s\-](\$\s\d+)\!*$/)
    md ||= name.match(/[\s\-](\d+[\,\.]\d+\s\$)\!*$/)
    md ||= name.match(/[\s\-](\d+\s\$)\!*$/)
    md
  end

  def self.run 
    ary = []
    arel = Post.where(:category_id => [3, 4, 5]).where('price IS NULL').order('id DESC')
    # arel = arel.where("name like '%$%' OR lower(name) like '%usd%'")
    arel.find_each(:batch_size => 2000) do |post|
      name = post.name
      next if name.blank?
      name = name.strip.downcase
      next if name.scan(/\$d+[\,\.]\d+/).size > 1
      next if name.scan(/\d+[\,\.]\d+\$/).size > 1
      next if name.scan(/\$\s\d+[\,\.]\d+/).size > 1
      next if name.scan(/\$\d+/).size > 1
      next if name.scan(/\d+\$/).size > 1
      next if (name.scan(/\d+\$/).size + name.scan(/\$\d+/).size) > 1
      next if name.scan(/\d+usd/).size > 1
      next if name.scan(/\d+\susd/).size > 1
      md = match_price(name)
      next unless md
      puts ". #{post.name} ==> #{md[1]}"
      post.price = md[1].gsub('$', '').gsub(',', '')
      post.changed? && post.save(:validate => false)
      ary << post.id
    end
    ary
  end
end
