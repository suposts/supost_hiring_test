Rollbar.configure do |config|
  config.access_token = Figaro.env.rollbar_access_token!

  config.verify_ssl_peer = false

  # Without configuration, Rollbar is enabled in all environments.
  # To disable in specific environments, set config.enabled=false.
  # Here we'll enable for only 'production' and 'staging':
  config.enabled = Rails.env.production? || Rails.env.staging?

  config.environment = ENV['ROLLBAR_ENV'] || Rails.env

  # Delayed Job
  config.use_delayed_job
  config.report_dj_data = false
end
