if Figaro.env.mailgun_username? && Figaro.env.mailgun_password?
  ActionMailer::Base.perform_deliveries    = true
  ActionMailer::Base.raise_delivery_errors = true

  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings   = {
    :address              => 'smtp.mailgun.org',
    :port                 => 587,
    :authentication       => :plain,
    :enable_starttls_auto => true,
    :domain               => 'supost.com',
    :user_name            => Figaro.env.mailgun_username,
    :password             => Figaro.env.mailgun_password
  }
end
