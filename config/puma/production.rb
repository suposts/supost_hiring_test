#!/usr/bin/env puma

# Start Puma with next command:
# RAILS_ENV=production bundle exec puma -e production -C ./config/puma/production.rb

root = File.expand_path('../../../', __FILE__)
rails_env = 'production'

directory       root
environment     rails_env
daemonize
pidfile         "#{root}/tmp/pids/puma.pid"
state_path      "#{root}/tmp/pids/puma.state"
stdout_redirect "#{root}/log/puma.access.log", "#{root}/log/puma.error.log", true
quiet
threads         5, 5
bind            "unix://#{root}/tmp/sockets/puma.sock"

workers 2

activate_control_app "unix://#{root}/tmp/sockets/pumactl.sock"

on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "#{root}/Gemfile"
end

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("#{root}/config/database.yml")[rails_env])
end
