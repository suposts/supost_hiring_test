#!/usr/bin/env puma

root = File.expand_path('../../../', __FILE__)
rails_env = 'development'

directory      root
environment    rails_env
daemonize      false
threads        1, 4
bind           'tcp://0.0.0.0:8080'
