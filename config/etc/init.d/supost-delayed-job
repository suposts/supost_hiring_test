#!/bin/sh
### BEGIN INIT INFO
# Provides:          supost-delayed-job
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Manage Delayed Job
# Description:       Start, stop, restart Delayed Job for supost application.
### END INIT INFO

APP_USER="deploy"
APP_ROOT=/home/$APP_USER/supost/current
RAILS_ENV="production"
PIDFILE=$APP_ROOT/tmp/pids/delayed_job.pid
RBENV_ROOT=/home/$APP_USER/.rbenv
RBENV_VERSION="2.0.0-p648"
PATH=$RBENV_ROOT/shims:$RBENV_ROOT/bin:/usr/local/bin:/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin
DESC="SUpost Delayed Job"
NAME=supost-delayed-job
SCRIPTNAME=/etc/init.d/$NAME
DAEMON="env RBENV_VERSION=$RBENV_VERSION RAILS_ENV=$RAILS_ENV bundle exec script/delayed_job"
DAEMON_ARGS="-n 2"
CMD="cd $APP_ROOT && $DAEMON $DAEMON_ARGS"

set -u

sig () {
    for PIDFILE in $APP_ROOT/tmp/pids/delayed_job.*.pid; do
        test -s "$PIDFILE" && kill -$1 `cat $PIDFILE`
    done
}

cleanup () {
    if [ -e $PIDFILE ]; then
        rm -f $PIDFILE
    fi
}

run () {
    if [ "$(id -un)" = "$APP_USER" ]; then
        eval $1
    else
        su -c "$1" - $APP_USER
    fi
}

case "$1" in
start)
    run "$CMD start"
    ;;
stop)
    run "$CMD stop"
    ;;
force-stop)
    sig TERM
    cleanup
    ;;
restart)
    run "$CMD restart"
    ;;
full-restart)
    run "$CMD stop"
    run "$CMD start"
    ;;
run)
    run "$CMD run"
    ;;
*)
    echo >&2 "Usage: $0 <start|stop|force-stop|restart|full-restart|run>"
    exit 1
    ;;
esac
