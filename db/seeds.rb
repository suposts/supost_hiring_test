class SupostSeeder
  def self.run
    puts "-> Create categories"

    categories = [
      [1,  "campus jobs",        "campus jobs"],
      [2,  "off campus jobs",    "off campus jobs"],
      [3,  "housing (offering)", "housing"],
      [4,  "housing (need)",     "need housing"],
      [5,  "for sale/wanted",    "for sale"],
      [6,  "resumes",            "resumes"],
      [7,  "services offered",   "services"],
      [8,  "personals/dating",   "personals"],
      [9,  "community",          "community"],
      [10, "events",             "events"]
    ]

    categories.each do |(id, name, short_name)|
      category = Category.find_by_id(id)
      unless category
        puts "-> Create Category (ID: #{id}, Name: #{name}, ShortName: #{short_name})"
        category            = Category.new
        category.id         = id
        category.name       = name
        category.short_name = short_name
        category.save
      end
    end

    puts "-> Create subcategories"

    subcategories = [
      [3,   5,  "barter"],
      [4,   5,  "bicycles"],
      [6,   5,  "books"],
      [7,   5,  "cars"],
      [8,   5,  "cds/dvds"],
      [9,   5,  "clothing & accessories"],
      [11,  5,  "computers & tech"],
      [12,  5,  "electronics"],
      [13,  5,  "free stuff"],
      [14,  5,  "furniture"],
      [17,  5,  "household items"],
      [18,  5,  "items wanted"],
      [23,  5,  "tickets"],
      [59,  3,  "rooms & shares"],
      [60,  3,  "apartments for rent"],
      [61,  3,  "housing swap"],
      [63,  3,  "storage & parking"],
      [66,  3,  "sublets & temporary"],
      [68,  4,  "apt/housing wanted"],
      [73,  7,  "computer services"],
      [80,  7,  "tutoring"],
      [90,  9,  "activity partners"],
      [92,  9,  "childcare"],
      [93,  9,  "general community"],
      [94,  9,  "groups"],
      [95,  9,  "local news and views"],
      [96,  9,  "lost & found"],
      [99,  9,  "politics"],
      [101, 9,  "volunteers"],
      [102, 9,  "classes"],
      [126, 10, "parties"],
      [127, 10, "talks"],
      [128, 9,  "religion"],
      [129, 10, "athletics"],
      [130, 8,  "friendship"],
      [131, 8,  "girl wants girl"],
      [132, 8,  "girl wants guy"],
      [133, 8,  "guy wants girl"],
      [134, 8,  "guy wants guy"],
      [135, 8,  "general romance"],
      [139, 10, "performances"],
      [140, 10, "conferences"],
      [141, 1,  "tutoring"],
      [142, 1,  "teaching"],
      [143, 1,  "admin"],
      [144, 1,  "research"],
      [146, 1,  "general"],
      [147, 7,  "general"],
      [148, 10, "general"],
      [149, 9,  "rideshare"],
      [150, 9,  "advice"],
      [151, 5,  "general"],
      [152, 4,  "room/share wanted"],
      [153, 4,  "sublet/temp wanted"],
      [154, 9,  "funny"],
      [155, 5,  "art + shop"],
      [156, 5,  "games"],
      [157, 2,  "unpaid interns"],
      [158, 2,  "general"],
      [159, 2,  "part-time"],
      [160, 2,  "paid interns"],
      [161, 2,  "full-time"]
    ]

    subcategories.each do |(id, category_id, name)|
      subcategory = Subcategory.find_by_id(id)
      unless subcategory
        puts "-> Create Subcategory (ID: #{id}, CategoryID: #{category_id}, Name: #{name})"
        subcategory             = Subcategory.new
        subcategory.id          = id
        subcategory.category_id = category_id
        subcategory.name        = name
        subcategory.save
      end
    end

    puts '-> Create Admin: nhphong1406@gmail.com'
    u = User.new(
      :first_name            => 'Phong',
      :last_name             => 'Nguyen',
      :email                 => 'nhphong1406@gmail.com',
      :password              => '123456',
      :password_confirmation => '123456'
    )
    u.user_type = 'Admin'
    u.save
  end
end

if Rails.env.development? || Rails.env.test? || Rails.env.staging?
  SupostSeeder.run
else
  puts 'This is for development only because it should be available on live server!'
end
