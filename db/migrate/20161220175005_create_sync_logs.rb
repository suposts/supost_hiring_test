class CreateSyncLogs < ActiveRecord::Migration
  def self.up
    create_table :sync_logs do |t|
      t.integer :payment_id
      t.integer :event_id

      t.timestamps
    end
  end

  def self.down
    drop_table :sync_logs
  end
end
