class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.integer :college_id
      t.integer :category_id
      t.integer :subcategory_id
      t.string  :email
      t.string  :ip
      t.string  :name
      t.text    :body
      t.integer :security_id
      t.integer :time_posted
      t.integer :time_modified
      t.string  :image_source1
      t.string  :image_source2
      t.string  :image_source3
      t.string  :image_source4
      t.integer :status
      t.string  :photo1_file_name
      t.string  :photo1_content_type
      t.string  :photo2_file_name
      t.string  :photo2_content_type
      t.string  :photo3_file_name
      t.string  :photo3_content_type
      t.string  :photo4_file_name
      t.string  :photo4_content_type
      t.timestamps
    end

    add_index :posts, :college_id
    add_index :posts, :status
    add_index :posts, :category_id
    add_index :posts, :subcategory_id
    add_index :posts, [:college_id, :status]
    add_index :posts, [:college_id, :status, :category_id]
    add_index :posts, [:college_id, :status, :subcategory_id]
  end

  def self.down
    drop_table :posts
  end
end
