class CreatePayments < ActiveRecord::Migration
  def self.up
    create_table :payments do |t|
      t.integer :post_id
      t.string  :seller
      t.string  :buyer
      t.decimal :price,              :precision => 10, :scale => 2
      t.integer :checkout_type
      t.decimal :amount,             :precision => 10, :scale => 2
      t.float   :rate
      t.date    :date
      t.string  :token
      t.string  :stripe_charge
      t.string  :stripe_source
      t.string  :stripe_transaction

      t.timestamps
    end

    add_index :payments, :post_id
    add_index :payments, :checkout_type
  end

  def self.down
    drop_table :payments
  end
end
