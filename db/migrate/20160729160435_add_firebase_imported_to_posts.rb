class AddFirebaseImportedToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :firebase_imported, :boolean, :default => false
  end

  def self.down
    remove_column :posts, :firebase_imported
  end
end
