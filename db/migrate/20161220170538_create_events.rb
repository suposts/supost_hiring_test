class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string  :stripe_id
      t.string  :stripe_object
      t.string  :stripe_type
      t.string  :api_version
      t.string  :created
      t.boolean :live_mode
      t.integer :pending_webbooks
      t.string  :request
      t.text    :data

      t.timestamps
    end

    add_index :events, :stripe_id
    add_index :events, :stripe_object
    add_index :events, :stripe_type
    add_index :events, [:stripe_id, :stripe_object, :stripe_type]
  end

  def self.down
    drop_table :events
  end
end
