class AddParseImportedToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :parse_imported, :boolean, :default => false
  end

  def self.down
    remove_column :posts, :parse_imported
  end
end
