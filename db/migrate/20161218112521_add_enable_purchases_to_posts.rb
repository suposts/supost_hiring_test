class AddEnablePurchasesToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :enable_purchases, :boolean, :default => false
  end

  def self.down
    remove_column :posts, :enable_purchases
  end
end
