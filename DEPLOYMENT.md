# SUpost Production Deployment

## Create and setup new droplet

Login into [DigitalOcean](https://digitalocean.com) and create new droplet as follows:

- Distributions: Ubuntu 14.04
- Size (Standard): 2 GB / 2 CPUs, 40 GB SSD disk and 3 TB transfer
- Datacenter region: San Francisco 1/2
- SSH keys: Your key, Greg's Key and Phong's key
- Number of Droplets: 1
- Hostname: supost.com

## Run setup script

Clone SUpost project

    $ git clone git@bitbucket.org:suposts/supost.git

Upload setup script `supost-ubuntu-14.04-setup.sh` from git repo to droplet

    $ cd supost
    $ scp supost-ubuntu-14.04-setup.sh root@<new-droplet-ip>:~

Login to droplet as `root` user and run setup script

    $ ssh root@<new-droplet-ip>
    $ bash supost-ubuntu-14.04-setup.sh

After complete setup, change password for `deploy` user

    $ passwd deploy

Reboot system

    $ sudo reboot

Upload `config/etc/nginx/nginx.conf` from git repo to droplet

    $ cd supost
    $ scp config/etc/nginx/nginx.conf root@<new-droplet-ip>:/etc/nginx/nginx.conf

Login to droplet as `root` user and restart nginx

    $ sudo /etc/init.d/nginx restart

## Setup MySQL database

Connect to mysql as `root` user

    $ mysql -uroot -p

Create `supost_prod` and `supost_staging` databases

    mysql> CREATE DATABASE `supost_staging` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
    mysql> CREATE DATABASE `supost_prod` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

Create new `deploy` user (replace 'SUd3p10y!' with new password if necessary)

    mysql> DROP USER 'deploy'@'localhost';
    mysql> CREATE USER 'deploy'@'localhost' IDENTIFIED BY 'SUd3p10y!';
    mysql> DROP USER 'deploy'@'%';
    mysql> CREATE USER 'deploy'@'%' IDENTIFIED BY 'SUd3p10y!';

Grant permissions

    mysql> GRANT ALL ON supost_staging.* to 'deploy'@'localhost';
    mysql> GRANT ALL ON supost_staging.* to 'deploy'@'%';
    mysql> GRANT ALL ON supost_prod.* to 'deploy'@'localhost';
    mysql> GRANT ALL ON supost_prod.* to 'deploy'@'%';
    mysql> FLUSH PRIVILEGES;

Verify user and permissions

    mysql> SHOW GRANTS FOR 'deploy'@'%';
    mysql> SHOW GRANTS FOR 'deploy'@'localhost';

Exit mysql

    mysql> exit

## Restore database

Before restoring database, you need to run urgent database backup on live server.

Login as `deploy` user

    $ ssh deploy@<new-droplet-ip>

Configure `awscli`

    $ aws configure
    AWS Access Key ID [None]: AKIAJNOUG5LSK62WXRQA
    AWS Secret Access Key [None]: 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
    Default region name [None]: us-east-1
    Default output format [None]: json

Find latest supost database backup (backups are stored at `s3://supost-backup/databases/<year>/<month>/`)

    $ aws s3 ls s3://supost-backup/databases/
    PRE 2016/
    PRE 2017/
    $ aws s3 ls s3://supost-backup/databases/2017/
    PRE 01/
    PRE 02/
    $ aws s3 ls s3://supost-backup/databases/2017/02/
    2017-02-01 01:02:44  116412082 supost_prod-20170201.sql.gz
    2017-02-02 01:02:47  116458253 supost_prod-20170202.sql.gz
    2017-02-03 01:02:55  116501823 supost_prod-20170203.sql.gz

Generate presigned S3 url for database backup `supost_prod-20170203.sql.gz` (link is expired in 2 hours - 7200 seconds)

    $ aws s3 presign s3://supost-backup/databases/2017/02/supost_prod-20170203.sql.gz --expires-in 7200
    https://supost-backup.s3.amazonaws.com/databases/2017/02/supost_prod-20170203.sql.gz?AWSAccessKeyId=AKIAJNOUG5LSK62WXRQA&Expires=1486137077&Signature=u7Z8fkkKKhh%2FkzMJ8KV%2BlOg6Osg%3D

Download database backup from presigned url

    $ wget "<presigned-url>" -O supost_prod-20170203.sql.gz

Restore database

    $ gunzip -c supost_prod-20161205.sql.gz | mysql -u deploy -p supost_prod

## Update new droplet's ip address for deployment

Go to git repo and update configs

    $ cd supost

Change ip address in `config/deploy/staging.rb`

    # Before
    server '104.216.116.132', user: 'deploy', roles: %w{app db web}, primary: true

    # After
    server '<new-droplet-ip>', user: 'deploy', roles: %w{app db web}, primary: true

Change ip address in `config/deploy/production.rb`

    # Before
    server '104.216.116.132', user: 'deploy', roles: %w{app db web}, primary: true

    # After
    server '<new-droplet-ip>', user: 'deploy', roles: %w{app db web}, primary: true

Change ip address in `config/etc/nginx/sites-available/supost-staging-puma`

    # Before
    server_name 104.216.116.132;

    # After
    server_name <new-droplet-ip>;

Change ip address in `config/etc/nginx/sites-available/supost-puma`

    # Before
    server_name www.supost.com supost.com 104.216.116.132;

    # After
    server_name www.supost.com supost.com <new-droplet-ip>

## Deployment

Go to git repo

    $ cd supost

Verify deployment setup

    $ bundle exec cap staging deploy:check
    $ bundle exec cap production deploy:check

Prepare config/application.yml for staging and production

    Consult Greg or Phong if you're deploying real site

Deploy new droplet

    $ bundle exec cap staging deploy
    $ bundle exec cap production deploy

Restart site

    $ bundle exec cap staging deploy:restart
    $ bundle exec cap production deploy:restart

## Restore photos

Copy photos from live server to new droplet

## Setup crontab

Login as `deploy` user

    $ ssh deploy@<new-droplet-ip>

Setup crontab

    $ crontab -e

Adds the flowling lines

    0    1 * * * /home/deploy/supost/current/cron/database-backup.sh
    */10 * * * * /home/deploy/supost/current/cron/parse-import.sh
    */5  * * * * /home/deploy/supost/current/cron/parse-update.sh
    */10 * * * * /home/deploy/supost/current/cron/firebase-import.sh
    */5  * * * * /home/deploy/supost/current/cron/firebase-update.sh
    */30 * * * * /home/deploy/supost/current/cron/remove-raw-photos.sh
    30   1 * * * /home/deploy/supost/current/cron/sync-photos.sh


